<?php


namespace App\Presenters;


use App\Model\Components\Form\AddExamForm;
use App\Model\Components\Form\CreateSubjectForm;
use App\Model\Entity\Exam;
use App\Model\Entity\Subject;
use App\Model\Mapper\ExamMapper;
use App\Model\Mapper\GroupMapper;
use App\Model\Mapper\SubjectMapper;
use App\Model\Service\SubjectService;
use Kdyby\Translation\Translator;
use MS\BasePresenter;
use MS\Components\MenuComponent\MenuComponent;
use MS\Entity\pages;
use Nette\Security\User;

class SubjectPresenter extends BasePresenter {
	private $subjectService;
	private $createSubjectForm;
	private $groupMapper;
	private $subjectMapper;
	private $addExamForm;
	private $examMapper;

	public function __construct(
		User $user,
		pages $pages,
		MenuComponent $menuComponent,
		Translator $translator, \
		AuthorizatorFactory $authorizatorFactory,
		SubjectService $subjectService,
		CreateSubjectForm $createSubjectForm,
		GroupMapper $groupMapper,
		SubjectMapper $subjectMapper,
		AddExamForm $addExamForm,
		ExamMapper $examMapper) {
		parent::__construct($user, $pages, $menuComponent, $translator, $authorizatorFactory);
		$this->subjectService = $subjectService;
		$this->createSubjectForm = $createSubjectForm;
		$this->groupMapper = $groupMapper;
		$this->subjectMapper = $subjectMapper;
		$this->addExamForm = $addExamForm;
		$this->examMapper = $examMapper;
	}

	//actions

	public function actionDefault() {
		$this->template->data = $this->subjectService->loadDataForDef($this->user->getId());
	}

	public function actionShow($id) {
		$this->securityCheck($id);

	}

	public function actionEdit($id) {
		$this->securityCheck($id);
		$this->template->data = $this->subjectService->loadByIdWithAllDependencies($id);
		$this->template->exams = $this->examMapper->loadAllByArgs(array('id_subject' => $id));
		$this->template->subject = $this->subjectMapper->loadById($id);
		$examGraphData = new \stdClass();
		$examGraphData->data = array();
		$examGraphData->labels = array();
		/** @var Exam $exam */
		foreach ($this->template->exams as $exam) {
			$examGraphData->data[] = \Helper::getPercent($exam->max_points,$exam->class_avarege);
			$examGraphData->labels[] = $exam->name;
		}
		$this->template->examGraphData = json_encode($examGraphData);
	}

	public function actionEditExam($idExam, $idSubject, $view = 0) {
		if ($view == 1) {
			$this->template->setFile(__DIR__ . '/templates/Subject/editExamClasic.latte');
		}
		$this->securityCheck($idSubject);
		$this->template->data = $this->subjectService->loadByIdWithAllDependencies($idSubject);
		$this->template->idExam = $idExam;

	}

	//actionsWithoutRender
	public function actionAddExam($idSubject) {
		$exam = new Exam();
		$exam->id_subject = $idSubject;
		$exam->name = "";
		$exam->max_points = 0;
		$this->examMapper->insetEntity($exam);
		$this->redirect('Subject:editExam', $this->examMapper->lastItem()->id, $idSubject);
	}

	public function actionDeleteExam($idExam,$idSubject) {
		$this->examMapper->destroyByArgs(['id'=>$idExam]);
		$this->successMessage();
		$this->redirect('Subject:edit',$idSubject);
	}

	public function ActionDeleteSubject($idSubject) {
		$this->subjectMapper->destroyByArgs(['id'=>$idSubject]);
		$this->successMessage();
		$this->redirect('Subject:');
	}

	//forms

	public function createComponentCreateSubjectForm() {
		if ($this->getParameter('id')) {
			$this->createSubjectForm->idSubject = $this->getParameter('id');
		}
		$this->createSubjectForm->defineForm();
		$this->createSubjectForm->onValidate[] = [$this, 'CreateSubjectFormSubmitted'];
		return $this->createSubjectForm;
	}

	public function CreateSubjectFormSubmitted($form, $values) {
		$uid = $this->createSubjectForm->save($values);
		$this->flashMessage($this->t('basic.success'), 'success');
		$this->redirect('Subject:edit', $uid);
	}

	public function createComponentAddExamForm() {
		$this->addExamForm->idExam = $this->getParameter('idExam');
		$this->addExamForm->idSubject = $this->getParameter('idSubject');
		$this->addExamForm->defineForm();
		$this->addExamForm->onValidate[] = [$this, 'addExamFormSubmitted'];
		return $this->addExamForm;
	}

	public function addExamFormSubmitted($form, $values) {
		try {
			$this->addExamForm->save($values);
			$this->successMessage();
			$this->redirect('Subject:edit', $this->getParameter('idSubject'));
		} catch (\Exception $exception) {
			throw $exception;
			$this->flashMessage($this->t('basic.error'), 'error');
		}
	}

	//other

	private function securityCheck($id) {
		/** @var Subject $subject */
		$subject = $this->subjectMapper->loadById($id);
		if (!$subject) {
			$this->throwBackCasueOfPrivilae('Group:');
		}

		if ($this->user->getId() != $subject->claims && !$this->user->isAllowed('group.editOther')) {
			$this->throwBackCasueOfPrivilae('Group:');
		}
	}
}