<?php


namespace App\Presenters;


use App\Model\Email\ExceptionEmail;
use App\Model\Email\InvitationEmail;
use App\Model\Entity\Invetition;
use App\Model\Mapper\InvetitionMapper;
use App\Model\Mapper\SubjectMapper;
use App\Model\Service\GivePrivilageService;
use App\Model\Service\PrivilegeService;
use Dibi\Exception;
use Dibi\UniqueConstraintViolationException;
use MS\Mappers\UserMapper;
use Nette\Application\Responses\JsonResponse;
use Nette\Application\UI\Presenter;

class ApiPresenter extends Presenter {
	private $userMapper;
	private $invitationEmail;
	private $privilegeService;
	private $givePrivilageService;
	private $subjectMapper;
	private $exceptionEmail;

	public function __construct(
		UserMapper $userMapper,
		InvitationEmail $invitationEmail,
		PrivilegeService $privilegeService,
		GivePrivilageService $givePrivilageService,
		SubjectMapper $subjectMapper,
		ExceptionEmail $exceptionEmail) {
		$this->userMapper = $userMapper;
		$this->invitationEmail = $invitationEmail;
		$this->privilegeService = $privilegeService;
		$this->givePrivilageService = $givePrivilageService;
		$this->subjectMapper = $subjectMapper;
		$this->exceptionEmail = $exceptionEmail;
	}

	public function startup() {
		parent::startup();
		if (!$this->user->isLoggedIn()) {
			$this->sendResponse(new JsonResponse(array('ok' => false)));
			exit();
		}
	}

	public function actionLoadUsers($query) {
		$response = array();
		foreach ($this->userMapper->searchForUsers($query) as $user) {
			$response[] = array(
				'value' => '',
				'label' => $user->username . ' | ' . $user->firstname . ' ' . $user->surname . '(' . $user->class . ')',
				'id' => $user->id,
				'customProperties' => $user->id
			);
		}

		$this->sendResponse(new JsonResponse($response));
	}

	public function ActionCreateAbstractUser($email) {
		if (!$this->user->isAllowed('user.create')) {
			$this->sendResponse(new JsonResponse(array('ok' => false, 'msg' => 'Nedostatečné oprávnění')));

		}
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->sendResponse(new JsonResponse(array('ok' => false, 'msg' => 'Invalidní email')));
		}
		try {
			$uid = $this->privilegeService->createAbstractUser($email)->id;
			$key = $this->privilegeService->createInvetitionKey($uid);
			$this->invitationEmail->send($email, $key);
			$this->sendResponse(new JsonResponse(array('ok' => true, 'msg' => 'Byl vytvořen nový uživatel a poslána pozvánka', 'uid' => $uid)));
		} catch (UniqueConstraintViolationException $e) {
			$this->sendResponse(new JsonResponse(array('ok' => false, 'msg' => 'Tento email je již v databázi.')));
		}
	}

	public function ActionGetSubject($idSubject) {
		$subject = $this->subjectMapper->loadById($idSubject);
		$this->sendResponse(new JsonResponse((array)$subject));
	}

	public function ActionSendException() {
		$files = scandir(__DIR__ . '/../../log', SCANDIR_SORT_DESCENDING);
		$this->exceptionEmail->send(file_get_contents(__DIR__ . '/../../log/' . $files[1]), $_POST['info']);
		$this->flashMessage('Informace o chybě byly odeslány', 'success');
		try {
			$this->redirect('Dashboard:');
		} catch (\Exception $e) {
			$this->sendResponse(new JsonResponse(['Nepodařilo se vás přesměrovat. Hlášení bylo úspěšně odesláno']));
		}
	}

}