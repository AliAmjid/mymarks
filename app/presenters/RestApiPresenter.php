<?php


namespace App\Presenters;


use App\Model\Components\UserForm\UserForm;
use Dibi\Exception;
use MS\Forms\LoginForm;
use MS\Mappers\UserMapper;
use Nette\Application\UI\Presenter;

class RestApiPresenter extends Presenter {

	private $loginForm;
	private $userMapper;
	public function __construct(
		LoginForm $loginForm,
		UserMapper $userMapper
	) {
		$this->response = new \stdClass();
		$this->loginForm = $loginForm;
		$this->userMapper = $userMapper;
	}

	private $response;
	protected function beforeRender() {
		$this->sendJson(json_encode((array)$this->response));
	}

	private function fetchData() {
		return json_decode($this->getHttpRequest()->getRawBody());
	}
	private function auth() {
		if (!$this->user->isLoggedIn()) {
			$this->getHttpResponse()->setCode('401');
			$this->sendJson(json_encode(array('err' => true)));
		}
	}

	public function actionLogin() {
		try {
			 $this->loginForm->submit($this->fetchData());
			 $this->response->key = $this->getSession()->getId();
			 $this->response->ok = true;
		} catch (\Exception $e) {
			$this->getHttpResponse()->setCode('401');
			$this->response->ok = false;
		}
	}


	public function actionTestAuth() {
		$this->response->msg = "Připojení je v pořádku.";
		if(!$this->user->isLoggedIn()) {
			$this->response->msg .= " Nejte autorizován.";
		} else {
			$this->response->msg .= " Jste autorizován.";
		}
	}

	public function actionGetUserInfo() {
		$this->response = $this->userMapper->loadById($this->user->getId());
	}
}