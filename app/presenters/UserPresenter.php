<?php

namespace App\Presenters;

use App\Model\Components\UserForm\UserForm;
use Kdyby\Translation\Translator;
use MS\BasePresenter;
use MS\Components\MenuComponent\MenuComponent;
use MS\Entity\pages;
use MS\Mappers\UserMapper;
use Nette\Application\Responses\JsonResponse;
use Nette\Application\UI\Form;
use Nette\Security\User;

class UserPresenter extends BasePresenter {
	protected $pagetitle = 'Uživatel';
	private $userForm;
	private $userId;
	private $userMapper;

	public function __construct(
		User $user,
		pages $pages,
		MenuComponent $menuComponent,
		Translator $translator,
		\AuthorizatorFactory $authorizatorFactory,
		UserForm $userForm,
		UserMapper $userMapper) {
		parent::__construct($user, $pages, $menuComponent, $translator, $authorizatorFactory);
		$this->userForm = $userForm;
		$this->userMapper = $userMapper;
	}

	//actions

	public function actionEdit($id) {

	}

	public function actionDefault($id) {
		$this->template->userRole = $this->userMapper->getUserRoleByName($id);
	}

	public function startup() {
		parent::startup();
		$this->common($this->getParameter('id'));
	}

	private function common($id) {
		if (!isset($id)) {
			$this->redirect('User:default', $this->user->getid());
		} else {
			if ($this->user->getId() != $id) {
				if (!$this->user->isAllowed('user.changeOtherUsers')) {
					$this->flashMessage($this->t('basic.noPrivilage'), 'error');
					$this->redirect('Dashboard:default');
				}
			}
		}
		if (!$this->userMapper->loadById($id)) {
			$this->flashMessage($this->t('basic.user.exist'), 'error');
			$this->redirect('Dashboard:default');
		}
		$this->userId = $id;
		$this->template->user = $this->userMapper->loadEntityById($id);
		$this->template->canChangeOthers = $this->user->isAllowed('user.changeOtherUsers');
	}

	public function createComponentUserForm() {
		$this->userForm->defineForm();
		$this->userForm->onValidate[] = [$this, 'UserFormSubmitted'];
		return $this->userForm;
	}

	public function UserFormSubmitted(UserForm $form, $values) {
		try {
			$form->save($values);
			$this->flashMessage($this->t('basic.forms.success'), 'success');
		} catch (\Exception $e) {
			$this->flashMessage($this->t('basic.forms.error'), 'error');
		}
		$this->redirect('User:default');
	}

	public function actionCreateAbstractUser($email) {
		$this->sendResponse(new JsonResponse(array('ok'=>true,'msg'=>'Done')));
	}

}