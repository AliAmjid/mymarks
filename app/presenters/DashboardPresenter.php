<?php


namespace App\Presenters;


use App\Model\Entity\Group;
use App\Model\Entity\Subject;
use App\Model\Entity\UserHasPoints;
use App\Model\Mapper\ExamMapper;
use App\Model\Mapper\GroupMapper;
use App\Model\Mapper\SubjectMapper;
use App\Model\Mapper\UserHasGroupMapper;
use App\Model\Mapper\UserHasPointsMapper;
use App\Model\Service\SubjectService;
use Kdyby\Translation\Translator;
use MS\BasePresenter;
use MS\Components\MenuComponent\MenuComponent;
use MS\Entity\pages;
use Nette\Security\User;
use Tracy\Debugger;

class DashboardPresenter extends BasePresenter {
	protected $pagetitle = "Hlavní stránka";
	private $articleMapper;
	private $subjectService;
	private $groupMapper;
	private $userHasGroupMapper;
	private $subjectMapper;
	private $userHasPointsMapper;
	private $examMapper;

	public function __construct(
		User $user,
		pages $pages,
		MenuComponent $menuComponent,
		Translator $translator,
		\AuthorizatorFactory $authorizatorFactory,
		SubjectService $subjectService,
		GroupMapper $groupMapper,
		UserHasGroupMapper $userHasGroupMapper,
		SubjectMapper $subjectMapper,
		UserHasPointsMapper $userHasPointsMapper,
		ExamMapper $examMapper) {
		parent::__construct($user, $pages, $menuComponent, $translator, $authorizatorFactory);
		$this->subjectService = $subjectService;
		$this->groupMapper = $groupMapper;
		$this->userHasGroupMapper = $userHasGroupMapper;
		$this->subjectMapper = $subjectMapper;
		$this->userHasPointsMapper = $userHasPointsMapper;
		$this->examMapper = $examMapper;
	}

	public function actionDefault() {
		$this->template->groups = $this->userHasGroupMapper->getGorups($this->user->getId());
		$subjects = array();
		/** @var Group $group $group */
		foreach ($this->template->groups as $group) {
			/** @var Subject $subject */
			foreach ($this->subjectMapper->loadAllByArgs(['id_group' => $group->id]) as $subject) {
				$subject->points = $this->userHasPointsMapper->getPointsOfUser($subject->id, $this->user->getId());
				$subject->maxPoints = $this->examMapper->loadMaxPointsForSubject($subject->id);
				$points = array();
				foreach ($this->userHasGroupMapper->loadAllByArgs(['id_group' => $group->id]) as $user) {
					$points[] = $this->userHasPointsMapper->getPointsOfUser($subject->id, $user->id_user);
				}
				$subject->percentil = \Helper::getPercentil($points, $subject->points);
				$subjects[] = $subject;
			}
		}
		$this->template->subjects = $subjects;
	}

	public function actionShow($idSubject) {
		/** @var Subject $subject */
		$subject = $this->subjectMapper->loadById($idSubject);
		$group = $this->groupMapper->loadById($subject->id_group);
		if (!$this->checkIfUserCanSee($group, $this->user->id)) {
			$this->throwBackCasueOfPrivilae();
		}
		$subject->points = $this->userHasPointsMapper->getPointsOfUser($subject->id, $this->user->getId());
		$subject->maxPoints = $this->examMapper->loadMaxPointsForSubject($subject->id);
		$points = array();
		foreach ($this->userHasGroupMapper->loadAllByArgs(['id_group' => $group->id]) as $user) {
			$points[] = $this->userHasPointsMapper->getPointsOfUser($subject->id, $user->id_user);
		}
		$subject->percentil = \Helper::getPercentil($points, $subject->points);
		$this->template->subject = $subject;
		$exams = $this->examMapper->loadAllByArgs(['id_subject' => $subject->id]);
		foreach ($exams as &$exam) {
			$points = array();
			/** @var UserHasPoints $point */
			foreach ($this->userHasPointsMapper->loadAllByArgs(['id_exam' => $exam->id]) as $point) {
				$points[] = $point->points;
			}
			$exam->points = $this->userHasPointsMapper->loadPointsForUser($exam->id, $this->user->getId());
			$exam->percentil = \Helper::getPercentil($points, $exam->points);
		}
		$this->template->exams = $exams;
	}

	private function checkIfUserCanSee($group, $userId) {
		foreach ($this->userHasGroupMapper->loadAllByArgs(['id_group' => $group->id]) as $user) {
			if ($userId == $user->id_user) {
				return true;
			}
		}
		return false;
	}

	public function ActionLogOut() {
		$this->user->logout();
		$this->redirect('Homepage:default');
	}


}