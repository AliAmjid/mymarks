<?php

namespace App\Presenters;

use App\Model\Email\Mail;
use App\Model\Entity\Group;
use App\Model\Entity\UserHasGroup;
use App\Model\Mapper\GroupMapper;
use App\Model\Mapper\UserHasGroupMapper;
use Dibi\Exception;
use Kdyby\Translation\Translator;
use Latte\Engine;
use MS\BasePresenter;
use MS\Components\MenuComponent\MenuComponent;
use MS\Entity\pages;
use MS\Entity\UserEntity;
use MS\Mappers\UserMapper;
use Nette\Application\Responses\JsonResponse;
use Nette\Mail\Message;
use Nette\Security\User;

class GroupPresenter extends BasePresenter {

	private $groupMapper;
	private $userHasGroupMapper;
	private $userMapper;

	public function __construct(
		User $user,
		pages $pages,
		MenuComponent $menuComponent,
		Translator $translator,
		\AuthorizatorFactory $authorizatorFactory,
		GroupMapper $groupMapper,
		UserHasGroupMapper $userHasGroupMapper,
		UserMapper $userMapper) {
		parent::__construct($user, $pages, $menuComponent, $translator, $authorizatorFactory);
		$this->groupMapper = $groupMapper;
		$this->userHasGroupMapper = $userHasGroupMapper;
		$this->userMapper = $userMapper;
	}

	public function actionDefault() {
		if (!$this->user->isAllowed('group')) {
			$this->throwBackCasueOfPrivilae();
		}

		$groups = $this->groupMapper->loadAll();
		$data = array();
		/** @var Group $group */
		foreach ($groups as $group) {
			$gr = new \stdClass();
			$gr->group = $group;
			$gr->created_by = $this->userMapper->loadById($group->created_by)->surname;
			$gr->count = $this->userHasGroupMapper->countUsers($group->id);
			$data[] = $gr;
		}
		$this->template->data = $data;

	}

	public function actionCreateNew() {
		if ($this->user->isAllowed('group.edit')) {
			$group = new Group();
			$group->name = 'Nepojmenovaná skupina';
			$group->created_by = $this->getUser()->getId();
			$group->copied_from = null;
			$this->groupMapper->insetEntity($group);
			$this->successMessage();
			$this->redirect('Group:edit', $this->groupMapper->lastItem()->id);
		}
		$this->throwBackCasueOfPrivilae('Group:');
	}

	public function actionEdit($id) {
		/** @var Group $group */
		$group = $this->groupMapper->loadById($id);
		$this->checkPrvilage($group);
		$this->template->group = $group;
		$this->template->users = $this->userHasGroupMapper->loadGroupsUsers($group->id);
	}

	private function checkPrvilage(Group $group) {
		if (!$this->user->isAllowed('group.edit')) {
			$this->throwBackCasueOfPrivilae();
		}
		if ($this->getUser()->getId() != $group->created_by && !$this->user->isAllowed('group.editAll')) {
			$this->throwBackCasueOfPrivilae('Group:');
		}
	}

	public function actionDelete($idGroup, $idUser) {
		$group = $this->groupMapper->loadById($idGroup);
		$this->checkPrvilage($group);
		try {
			$this->userHasGroupMapper->delteUser($idGroup, $idUser);
			$this->successMessage();
		} catch (Exception $exception) {
			$this->flashMessage('basic.error', 'error');
			throw $exception;
		}
		$this->redirect('Group:edit', $idGroup);
	}

	public function actionDeleteGroup($id) {
		$group = $this->groupMapper->loadById($id);
		$this->checkPrvilage($group);
		try {
			$this->groupMapper->destroy($group);
			$this->flashMessage($this->t('basic.success'), 'success');

		} catch (\Exception $exception) {
			$this->flashMessage($this->t('basic.error'), 'error');
		}
		$this->redirect('Group:');
	}


	public function ActionAddUser($idUser, $idGroup) {
		$response = array();
		$group = $this->groupMapper->loadById($idGroup);
		$this->checkForApi($idUser, $idGroup, $group);
		/** @var UserEntity $user */
		$user = $this->userMapper->loadById($idUser);
		try {
			/** @var UserEntity $user */
			$user = $this->userMapper->loadById($idUser);

			$userHasGroup = new UserHasGroup();
			$userHasGroup->id_user = $user->id;
			$userHasGroup->id_group = $group->id;
			$this->userHasGroupMapper->insetEntity($userHasGroup);
		} catch (\Exception $exception) {
			$response = array('ok' => false, 'msg' => 'Došlo k chybě při ukládání dat do databáze. Chyba může být kvůli pokusu o přidání neexstujícího uživatele.');
			$this->sendResponse(new JsonResponse($response));
		}

		$response = array('ok' => true, 'user' => [
			$user->firstname . ' ' . $user->surname,
			$user->class,
			$user->username,
			'<a href="/group/delete?idGroup=' . $idGroup . '&idUser=' . $idUser . '" class="btn-icon action-delete"><i class="fas fa-trash-alt"></i></a>'
		]);

		$this->sendResponse(new JsonResponse($response));
	}

	private function checkForApi($idUser, $idGroup, $group = "") {
		if (empty($group)) {
			$group = $this->groupMapper->loadById($idGroup);
		}
		$response = array();
		if ($this->userHasGroupMapper->countByUserId($idUser, $idGroup) != 0) {
			$response = array('ok' => false, 'msg' => 'Tento uživatel byl již přidán!');
			$this->sendResponse(new JsonResponse($response));
		}
		if (empty($idUser) || empty($idUser)) {
			$response = array('ok' => false, 'msg' => 'Došlo k chybě při čtení HTTP hlavičky - client poslal neúplné informace.');
			$this->sendResponse(new JsonResponse($response));
		}
		if (!$this->user->isAllowed('group.edit')) {
			$response = array('ok' => false, 'msg' => 'Nemáte povolení k editaci této skupiny');
			$this->sendResponse(new JsonResponse($response));

		}
		if ($this->getUser()->getId() != $group->created_by && !$this->user->isAllowed('group.editAll')) {
			$response = array('ok' => false, 'msg' => 'Nemáte povolení k editaci této skupiny');
			$this->sendResponse(new JsonResponse($response));
		}
	}

	public function actionRename($idGroup, $name) {
		$group = $this->groupMapper->loadById($idGroup);
		if (!$this->user->isAllowed('group.edit')) {
			$response = array('ok' => false, 'msg' => 'Nemáte povolení k editaci této skupiny');
			$this->sendResponse(new JsonResponse($response));

		}
		if ($this->getUser()->getId() != $group->created_by && !$this->user->isAllowed('group.editAll')) {
			$response = array('ok' => false, 'msg' => 'Nemáte povolení k editaci této skupiny');
			$this->sendResponse(new JsonResponse($response));
		}
		$group->name = $name;
		try {
			$this->groupMapper->updateEntity($group);
			$response = array('ok' => true, 'msg' => $group->name);
		} catch (\Exception $exception) {
			$response = array('ok' => false, 'msg' => 'Došlo k chybě při ukládání dat');
		}
		$this->sendResponse(new JsonResponse($response));

	}
}