<?php


namespace App\Presenters;


use Kdyby\Translation\Translator;
use MS\BasePresenter;
use MS\Components\MenuComponent\MenuComponent;
use MS\Entity\AclResourceEntity;
use MS\Entity\AclRoleHasResourceEntity;
use MS\Entity\pages;
use MS\Mappers\AclResourceMapper;
use MS\Mappers\AclRoleHasResourceMapper;
use MS\Mappers\AclRoleMapper;
use Nette\Application\Responses\JsonResponse;
use Nette\Security\User;
use Tracy\Debugger;

class SettingsPresenter extends BasePresenter {
	private $aclRoleHasResourceMapper;
	private $aclRoleMapper;
	private $aclResourceMapper;

	public function __construct(
		User $user,
		pages $pages,
		MenuComponent $menuComponent,
		Translator $translator,
		\AuthorizatorFactory $authorizatorFactory,
		AclRoleHasResourceMapper $aclRoleHasResourceMapper,
		AclRoleMapper $aclRoleMapper,
		AclResourceMapper $aclResourceMapper) {
		parent::__construct($user, $pages, $menuComponent, $translator, $authorizatorFactory);
		$this->aclRoleHasResourceMapper = $aclRoleHasResourceMapper;
		$this->aclRoleMapper = $aclRoleMapper;
		$this->aclResourceMapper = $aclResourceMapper;
	}

	public function actionDefault() {
		$this->template->roles = $this->aclRoleMapper->loadAll();
		/** @var AclResourceEntity $resource */
		$r = array();
		foreach ($this->aclResourceMapper->loadByName() as $resource) {
			$resource->roles = $this->getRoles($resource->id);
			$r[] = $resource;
		}
		foreach ($r as $resource) {
			Debugger::barDump($resource->roles);
		}
		$this->template->resources = $r;
	}

	public function actionSwitch($idResource, $idRole) {
		if ($this->aclRoleHasResourceMapper->checkIfResourceExist($idResource, $idRole)) {
			$this->aclRoleHasResourceMapper->destroyByArgs([
				'id_acl_role' => $idRole,
				'id_acl_resource' => $idResource
			]);
			$this->sendResponse(new JsonResponse(array('ok' => true, 'msg' => 'Právo odebráno', 'class'=>'disabled')));
		} else {
			$acl = new AclRoleHasResourceEntity();
			$acl->id_acl_role = $idRole;
			$acl->id_acl_resource = $idResource;
			$this->aclRoleHasResourceMapper->insetEntity($acl);
			$this->sendResponse(new JsonResponse(array('ok' => true, 'msg' => 'Právo přidáno', 'class'=>'enabled')));

		}

	}

	private function getRoles($idResource) {
		$roles = array();
		foreach ($this->aclRoleMapper->loadAll() as $role) {
			$role->idRe = $idResource;
			$role->roId = $role->id;
			$role->isEnabled = $this->aclRoleHasResourceMapper->checkIfResourceExist($idResource, $role->id) ? true : false;
			$roles[] = $role;
		}
		return $roles;
	}

}