<?php


use MS\Entity\AclResourceEntity;
use MS\Mappers\AclResourceMapper;
use MS\Mappers\AclRoleHasResourceMapper;
use MS\Mappers\AclRoleMapper;
use MS\Mappers\UserHasRoleMapper;
use MS\Mappers\UserMapper;
use Nette\Security\Permission;

class AuthorizatorFactory extends Permission {

	public $aclResourceMapper;
	public $aclRoleMapper;
	public $aclRoleHasResourceMapper;
	public $userMapper;
	public $userHasRoleMapper;

	public function __construct(
		AclResourceMapper $aclResourceMapper,
		AclRoleMapper $aclRoleMapper,
		AclRoleHasResourceMapper $aclRoleHasResourceMapper,
		UserMapper $userMapper,
		UserHasRoleMapper $userHasRoleMapper
	) {
		$this->aclResourceMapper = $aclResourceMapper;
		$this->aclRoleMapper = $aclRoleMapper;
		$this->aclRoleHasResourceMapper = $aclRoleHasResourceMapper;
		$this->userMapper = $userMapper;
		$this->userHasRoleMapper = $userHasRoleMapper;

		$resources = $this->aclResourceMapper->loadAll();
		/** @var AclResourceEntity $resource */
		foreach ($resources as $resource) {
			parent::addResource($resource->name);
		}

		$roles = $this->aclRoleMapper->loadAll();
		foreach ($roles as $role) {
			parent::addRole($role->name);
			$resourcesId = $this->aclRoleHasResourceMapper->loadByRoleId($role->id);
			foreach ($resourcesId as $resourceId) {
				$resource = $this->aclResourceMapper->loadEntityById($resourceId->id_acl_resource);
				parent::allow($role->name, $resource->name);
			}
		}
	}
}