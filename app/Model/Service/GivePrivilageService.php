<?php


namespace App\Model\Service;


use MS\Entity\UserEntity;
use MS\Entity\UserHasRoleEntity;
use MS\Mappers\AclRoleMapper;
use MS\Mappers\UserHasRoleMapper;

class GivePrivilageService {

	private $user;
	private $teacher = 1;
	private $developer = 2;
	private $student = 3;
	private $userHasRoleMapper;

	public function __construct(UserHasRoleMapper $userHasRoleMapper) {
		$this->userHasRoleMapper = $userHasRoleMapper;
	}

	public function set($user) {
		if (is_array($user)) {
			$id = $user['id'];
		} elseif ($user instanceof UserEntity) {
			$id = $user->id;
		} else {
			$id = $user;
		}
		$this->user = $id;
		return $this;
	}

	public function Student() {
		$this->common($this->student);
	}

	public function Developer() {
		$this->common($this->developer);

	}

	public function Teacher() {
		$this->common($this->teacher);

	}

	private function common($idRole) {
		$idUser = $this->user;
		$entity = new UserHasRoleEntity();
		$entity->id_user = $idUser;
		$entity->id_acl_role = $idRole;
		if ($this->userHasRoleMapper->countByIdUser($idUser) > 0) {
			$this->userHasRoleMapper->updateEntity(array('id_user' => $idUser));
		} else {
			$this->userHasRoleMapper->insetEntity($entity);
		}
	}
}