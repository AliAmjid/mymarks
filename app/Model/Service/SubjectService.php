<?php


namespace App\Model\Service;


use App\Model\Entity\Group;
use App\Model\Entity\Subject;
use App\Model\Entity\UserHasGroup;
use App\Model\Mapper\ExamMapper;
use App\Model\Mapper\GroupMapper;
use App\Model\Mapper\SubjectMapper;
use App\Model\Mapper\UserHasGroupMapper;
use App\Model\Mapper\UserHasPointsMapper;
use Tracy\Debugger;

class SubjectService {
	private $subjectMapper;
	private $groupMapper;
	private $userHasGroupMapper;
	private $userHasPointsMapper;
	private $examMapper;
	public function __construct(
		SubjectMapper $subjectMapper,
		GroupMapper $groupMapper,
		UserHasGroupMapper $userHasGroupMapper,
		UserHasPointsMapper $userHasPointsMapper,
		ExamMapper $examMapper) {

		$this->groupMapper = $groupMapper;
		$this->subjectMapper = $subjectMapper;
		$this->userHasGroupMapper = $userHasGroupMapper;
		$this->userHasPointsMapper = $userHasPointsMapper;
		$this->examMapper = $examMapper;
	}


	public function loadDataForDef($userId) {
		$return = array();
		$subjects = $this->subjectMapper->loadAllByArgs(array('claims' => $userId));
		/** @var Subject $subject */
		foreach ($subjects as $subject) {
			$data = new \stdClass();
			$data->subject = $subject;
			$data->group = $this->groupMapper->loadById($subject->id_group);
			$return[] = $data;
		}
		return $return;
	}

	public function loadByIdWithAllDependencies($id) {
		/** @var Subject $subject */
		$subject = $this->subjectMapper->loadById($id);
		$group = $this->groupMapper->loadById($subject->id_group);
		$group->users = $this->userHasGroupMapper->loadAllByArgs(array('id_group' => $group->id), 'LEFT JOIN user ON user_has_group.id_user = user.id');
		$subject->id_group = $group;
		$subject->group = $group;
		$subject->maxPoints = $this->examMapper->loadMaxPointsForSubject($subject->id);
		foreach ($group->users as &$user) {
			$user->points = $this->userHasPointsMapper->getPointsOfUser($id, $user->id);
		}
		return $subject;
	}

	public function loadPointsForExam($idExam) {
		return $this->userHasPointsMapper->loadPointsForAll($idExam);
	}

}