<?php


namespace App\Model\Email;


use Latte\Engine;
use Nette\Mail\Message;

class InvitationEmail extends Mail {

	public function send($email, $invitationKey) {
		$args = [
			'url' => 'http://' . $this->httpRequest->getUrl()->host,
			'link' => 'http://' . $this->httpRequest->getUrl()->host . "/homepage/register?id=" . $invitationKey];
		$latte = new Engine();
		$mail = new Message();
		$mail->setFrom('noreply@mymarks.com', 'MyMarks')
			->addTo($email)
			->setHtmlBody($latte->renderToString(__DIR__ . '/templates/invetition.latte', $args));
		$this->sendEmail($mail);
	}
}