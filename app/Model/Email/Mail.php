<?php

namespace App\Model\Email;

use http\Env\Response;
use Nette\DI\Container;
use Nette\Mail\SmtpMailer;
use Nette\Http\Request;

class Mail {
	public $mailer;
	protected $httpRequest;
	public function __construct(Container $container, Request $httpRequest) {
		$args = $container->getParameters()['smpt'];
		$this->mailer = new SmtpMailer($args);
		$this->httpRequest = $httpRequest;
	}

	public function sendEmail($email) {
		$this->mailer->send($email);
	}

}