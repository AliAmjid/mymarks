<?php


namespace App\Model\Email;

use Latte\Engine;
use Nette\Mail\Message;

class ExceptionEmail extends Mail {
	public function send($attachment,$info)
	{
		$latte = new Engine();
		$mail = new Message();
		$mail->setFrom('noreply@mymarks.com', 'MyMarks')
			->addTo('alih.amjid126@gmail.com')
			->setHtmlBody($latte->renderToString(__DIR__ . '/templates/exception.latte',['info'=>$info]));
		$mail->addAttachment('exception.html',$attachment);
		$this->sendEmail($mail);
	}
}