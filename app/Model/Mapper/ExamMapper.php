<?php
namespace App\Model\Mapper;

use App\Model\Entity\Exam;
use MS\Mappers\Mapper;
use Nette\DI\Container;

class ExamMapper extends Mapper {
	protected $tableName = "exam";
	protected $entity;

	public function __construct(Container $container, Exam $entity) {
		parent::__construct($container);
		$this->entity = $entity;
	}

	public function loadMaxPointsForSubject($idSubject) {
		return $this->query('SELECT SUM(max_points) as max_points FROM %n WHERE id_subject = %i',$this->tableName,$idSubject)->fetchSingle();
	}

}