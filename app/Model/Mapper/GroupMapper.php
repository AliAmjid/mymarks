<?php

namespace App\Model\Mapper;

use App\Model\Entity\Group;
use MS\Mappers\Mapper;
use Nette\DI\Container;

class GroupMapper extends Mapper {
	protected $tableName = "group";
	protected $entity;

	public function __construct(Container $container, Group $entity) {
		parent::__construct($container);
		$this->entity = $entity;
	}


	//Zkurveny sql uvozovky :)))))

	public function insetEntity($entity) {
		$this->query('INSERT INTO `group` %v', (array)$entity);
	}

	public function loadEntityById(int $id) {
		return $this->mapOne($this->query('SELECT * FROM `group` WHERE id = %i', $id)->fetch());
	}

	protected function mapOne($row) {
		$entity = clone $this->entity;
		foreach (array_keys(get_object_vars($this->entity)) as $item) {
			if (!$row) {
				return null;
			}
			$entity->$item = $row->$item;
		}
		return $entity;
	}

	public function loadAll() {
		$all = $this->query('SELECT * FROM `group`');
		$rows = array();
		foreach ($all as $row) {
			$rows[] = $this->mapOne($row);
		}
		return $rows;
	}


	public function updateEntity($entity, $where = null) {
		if (!$where) {
			$this->query('UPDATE `group` SET %a WHERE id = %i', (array)$entity, $entity->id);
		} else {
			$this->query('UPDATE `group` SET %a WHERE %and', (array)$entity, $where);
		}

	}

	public function loadById($id) {
		return $this->mapOne($this->query('SELECT * FROM `group` WHERE id = %i', $id)->fetch());
	}

	public function lastItem() {
		return $this->mapOne($this->query('SELECT * FROM `group` ORDER BY id DESC LIMIT 1')->fetch());
	}

}