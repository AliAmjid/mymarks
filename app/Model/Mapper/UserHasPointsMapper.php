<?php

namespace App\Model\Mapper;

use App\Model\Entity\Subject;
use App\Model\Entity\UserHasPoints;
use MS\Mappers\Mapper;
use Nette\DI\Container;

class UserHasPointsMapper extends Mapper {
	protected $tableName = "user_has_points";
	protected $entity;

	public function __construct(Container $container, UserHasPoints $entity) {
		parent::__construct($container);
		$this->entity = $entity;
	}

	public function loadMainViewForExam($idExam) {
		$this->dataSource('SELECT * FROM user_has_points uhp LEFT JOIN exam ON exam.id = uhp.id_exam WHERE exam.id = %i', $idExam)->toFluent();
	}

	public function loadPointsForAll($idExam) {
		return $this->query('SELECT * FROM user_has_points uhp LEFT JOIN exam ON exam.id = uhp.id_exam WHERE exam.id = %i', $idExam)->fetchAll();
	}

	public function loadPointsForUser($idExam, $idUser) {
		return $this->query('SELECT points FROM user_has_points WHERE id_user = %i AND id_exam = %i ', $idUser, $idExam)->fetchSingle();
	}

	public function deleteAllByExam($idExam) {
		$this->query('DELETE FROM %n WHERE id_exam = %i', $this->tableName, $idExam);
	}

	public function getPointsOfUser($idSubject,$idUser) {
		return $this->query('SELECT SUM(points) as points FROM user_has_points WHERE id_subject = %i AND id_user = %i',$idSubject,$idUser)->fetchSingle();
	}
}