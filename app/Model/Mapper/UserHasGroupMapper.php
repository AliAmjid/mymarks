<?php

namespace App\Model\Mapper;

use App\Model\Entity\Group;
use App\Model\Entity\UserHasGroup;
use MS\Mappers\Mapper;
use Nette\DI\Container;

class UserHasGroupMapper extends Mapper {
	protected $tableName = "user_has_group";
	protected $entity;

	public function __construct(Container $container, UserHasGroup $entity) {
		parent::__construct($container);
		$this->entity = $entity;
	}

	public function loadGroupsUsers($idGroup) {
		return $this->query('
			SELECT user_has_group.*,
			 user.surname,
			 user.firstname,
			 user.class,
			 user.username
 			 FROM %n 
 			 LEFT JOIN user ON user.id = user_has_group.id_user 
 			 WHERE user_has_group.id_group = ', $this->tableName, $idGroup)->fetchAll();
	}

	public function countByUserId($userId, $idGroup) {
		return $this->query('SELECT * FROM %n WHERE id_user = %i AND id_group = %i', $this->tableName, $userId, $idGroup)->count();
	}

	public function countUsers($idGroup) {
		return $this->query('SELECT * FROM %n WHERE id_group = %i', $this->tableName, $idGroup)->count();
	}

	public function delteUser($idGroup, $idUser) {
		$this->query('DELETE FROM %n WHERE id_user = %i AND id_group =%i', $this->tableName, $idUser, $idGroup)->count();
	}

	public function getGorups($idUser) {
		return $this->query('SELECT gr.* FROM %n uhg LEFT JOIN `group` gr on gr.id = uhg.id_group WHERE uhg.id_user = %i', $this->tableName, $idUser)->fetchAll();
	}

}