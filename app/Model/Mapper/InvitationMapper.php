<?php


namespace App\Model\Mapper;


use App\Model\Entity\invitation;
use MS\Mappers\Mapper;
use Nette\DI\Container;

class InvitationMapper extends Mapper {
	protected $tableName = 'invitation';

	public function __construct(Container $container, invitation $invetition) {
		parent::__construct($container);
		$this->entity = $invetition;
	}
}