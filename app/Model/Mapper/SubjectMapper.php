<?php

namespace App\Model\Mapper;

use App\Model\Entity\Subject;
use MS\Mappers\Mapper;
use Nette\DI\Container;

class SubjectMapper extends Mapper {
	protected $tableName = "subject";
	protected $entity;

	public function __construct(Container $container, Subject $entity) {
		parent::__construct($container);
		$this->entity = $entity;
	}

}