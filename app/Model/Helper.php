<?php


use Tracy\Debugger;

class Helper {
	static function getMark($subject, $maxPoints, $points, $round = true) {
		$mark = 5;
		$percent = Helper::getPercent($maxPoints, $points, $round);
		if ($percent >= $subject->cr_4) {
			$mark = 4;
		}
		if ($percent >= $subject->cr_3) {
			$mark = 3;
		}
		if ($percent >= $subject->cr_2) {
			$mark = 2;
		}
		if ($percent >= $subject->cr_1) {
			$mark = 1;
		}
		return $mark;
	}

	static function getPercent($maxPoints, $points, $round = true) {
		if ($maxPoints == 0) {
			return 100;
		}
		$percent = (100 / $maxPoints) * $points;
		return $round ? round($percent, '2') : $percent;
	}

	static function getAveregeMark($maxPoints, $points, $round = true) {
		$percent = Helper::getPercent($maxPoints, $points);
		$result = 6 - (0.05 * $percent);
		return $round ? round($result,2) : $result;
	}

	static function getColor($mark) {
		$colors = ['#9aff56', '#60a531', '#f3f725', '#efc017', '#ff3700'];
		$m = $mark - 1;
		$color = $colors[$m];
		unset($mark);
		unset($m);
		return $color;
	}

	static function hash() {
		return '$2y$10$rzr1in5P8zMpeZPQ46dJY.xt0dloKzAlBksqrpCDypJkK3SINXaK6';
	}

	static function getPercentil(array $array, $percentil) {
		$vars = [];
		foreach ($array as $value) {
			if ($value <= $percentil) {
				$vars[] = $value;
			}
		}
		$result = (100/count($array)) * count($vars);
		return round($result,2);
	}
}