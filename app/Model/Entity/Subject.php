<?php


namespace App\Model\Entity;


class Subject {
	public $id;
	public $name;
	public $claims;
	public $id_group;
	public $class_average;
	public $cr_1;
	public $cr_2;
	public $cr_3;
	public $cr_4;
}