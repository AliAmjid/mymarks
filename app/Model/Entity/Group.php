<?php

namespace App\Model\Entity;

class Group {
	public $id;
	public $name;
	public $created_by;
	public $copied_from;
}