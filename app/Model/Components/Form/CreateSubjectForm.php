<?php

namespace App\Model\Components\Form;

use App\Model\Entity\Group;
use App\Model\Entity\Subject;
use App\Model\Mapper\GroupMapper;
use App\Model\Mapper\SubjectMapper;
use Kdyby\Translation\Translator;
use MS\Forms\BaseForm;
use MS\Forms\CustomeFormRenderer;
use Nette\Security\User;

class CreateSubjectForm extends BaseForm {
	private $subjectMapper;
	private $groupMapper;
	public $idSubject = null;

	public function __construct(
		CustomeFormRenderer $customeFormRenderer,
		Translator $translator,
		User $user,
		SubjectMapper $subjectMapper,
		GroupMapper $groupMapper) {
		parent::__construct(null, null, $customeFormRenderer, $translator, $user);
		$this->subjectMapper = $subjectMapper;
		$this->groupMapper = $groupMapper;
	}

	public function defineForm() {
		$this->addText('name', 'Jméno předmětu');
		$this->addSelect('group', 'Zvolate skupinu', $this->loadGroupes());
		$this->addText('cr1', 'Převod pro 1 v % >=')->setDefaultValue(85)->setAttribute('type', 'number');
		$this->addText('cr2', 'Převod pro 2 v % >=')->setDefaultValue(70)->setAttribute('type', 'number');
		$this->addText('cr3', 'Převod pro 3 v % >=')->setDefaultValue(55)->setAttribute('type', 'number');
		$this->addText('cr4', 'Převod pro 4 v % >=')->setDefaultValue(40)->setAttribute('type', 'number');
		$this->addSubmit('submit');
		if ($this->idSubject) {
			/** @var Subject $subject */
			$subject = $this->subjectMapper->loadById($this->idSubject);
			$this->setDefaults(
				array(
					'name' => $subject->name,
					'group' => $subject->id_group,
					'cr1' => $subject->cr_1,
					'cr2' => $subject->cr_2,
					'cr3' => $subject->cr_3,
					'cr4' => $subject->cr_4,
				)
			);
		}
	}

	public function save($values) {
		$subject = new Subject();
		$subject->claims = $this->user->getId();
		$subject->id_group = $values->group;
		$subject->name = $values->name;
		$subject->cr_1 = $values->cr1;
		$subject->cr_2 = $values->cr2;
		$subject->cr_3 = $values->cr3;
		$subject->cr_4 = $values->cr4;
		if ($this->idSubject) {
			$subject->id = $this->idSubject;
			$this->subjectMapper->updateEntity($subject);
			return $subject->id;
		} else {
			$this->subjectMapper->insetEntity($subject);
			return $this->subjectMapper->lastItem()->id;
		}
	}

	private function loadGroupes() {
		$data = array();
		$groups = $this->groupMapper->loadAll();
		/** @var Group $group */
		foreach ($groups as $group) {
			$data[$group->id] = $group->name;
		}
		return $data;

	}

}