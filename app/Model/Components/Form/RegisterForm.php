<?php


namespace App\Model\Components\Form;


use App\Model\Entity\Invitation;
use App\Model\Mapper\InvitationMapper;
use App\Model\Service\GivePrivilageService;
use Kdyby\Translation\Translator;
use MS\Entity\UserEntity;
use MS\Forms\BaseForm;
use MS\Forms\CustomeFormRenderer;
use MS\Mappers\UserMapper;
use Nette;

class RegisterForm extends BaseForm {
	private $userMapper;
	private $invitationMapper;
	public $key;
	private $authenticator;
	private $givePrivilageService;

	public function __construct(
		CustomeFormRenderer $customeFormRenderer,
		Translator $translator,
		Nette\Security\User $user,
		UserMapper $userMapper,
		InvitationMapper $invitationMapper,
		GivePrivilageService $givePrivilageService,
		\Authenticator $authenticator) {
		parent::__construct(null, null, $customeFormRenderer, $translator, $user);
		$this->userMapper = $userMapper;
		$this->invitationMapper = $invitationMapper;
		$this->authenticator = $authenticator;
		$this->givePrivilageService = $givePrivilageService;
	}

	public function defineForm() {
		/** @var Invitation $invitation */
		$invitation = $this->invitationMapper->loadByArgs(array('key' => $this->key));
		$user = $this->userMapper->loadById($invitation->has_abstract_user);
		$this->addEmail('username', 'Email')->setDisabled(true)->setValue($user->username);
		$this->addHidden('key', $this->key);
		$this->addText('firstname', 'Křestní jméno');
		$this->addText('surname', 'Přijmení');
		$this->addText('class', 'Třída (npr. 1.B)');
		$this->addPassword('password', 'Heslo');
		$this->addSubmit('submit');
		return $this;
	}

	public function save($values) {
		/** @var Invitation $invitation */
		$invitation = $this->invitationMapper->loadByArgs(array('key' => $values->key));
		/** @var UserEntity $user */
		$user = $this->userMapper->loadById($invitation->has_abstract_user);
		$user->password = Nette\Security\Passwords::hash($values->password);
		$user->class = $values->class;
		$user->firstname = $values->firstname;
		$user->surname = $values->surname;
		$user->isAbstract = 0;
		$this->userMapper->updateEntity($user);
		$this->invitationMapper->destroyByArgs(['key' => $values->key]);
		$this->givePrivilageService->set($user->id)->Student();
		$this->user->login($this->authenticator->loginWithoutAuth($user));
	}
}