<?php


namespace App\Model\Components\Form;


use App\Model\Entity\Exam;
use App\Model\Entity\Subject;
use App\Model\Entity\UserHasPoints;
use App\Model\Mapper\ExamMapper;
use App\Model\Mapper\SubjectMapper;
use App\Model\Mapper\UserHasPointsMapper;
use App\Model\Service\SubjectService;
use Kdyby\Translation\Translator;
use MS\Entity\UserEntity;
use MS\Forms\BaseForm;
use MS\Forms\CustomeFormRenderer;
use Nette;

class AddExamForm extends BaseForm {
	private $subjectService;
	public $idSubject;
	public $idExam;
	private $examMapper;
	private $userHasPointsMapper;

	public function __construct(
		CustomeFormRenderer $customeFormRenderer,
		Translator $translator,
		Nette\Security\User $user,
		SubjectService $subjectService,
		ExamMapper $examMapper,
		UserHasPointsMapper $userHasPointsMapper) {
		parent::__construct(null, null, $customeFormRenderer, $translator, $user);
		$this->subjectService = $subjectService;
		$this->examMapper = $examMapper;
		$this->userHasPointsMapper = $userHasPointsMapper;
	}

	public function defineForm() {
		/** @var Exam $exam */
		$exam = $this->examMapper->loadById($this->idExam);
		$data = $this->subjectService->loadByIdWithAllDependencies($this->idSubject);
		$this->addText('name', 'Jméno testu / popis')->setDefaultValue($exam->name);
		$this->addText('max_points', 'Maximální počet bodů')->setDefaultValue($exam->max_points)->setAttribute('type', 'number');
		$this->addHidden('idExam')->setValue($exam->id);
		$this->addHidden('idSubject')->setValue($this->idSubject);

		/** @var UserEntity $user */
		foreach ($data->group->users as $user) {
			$username = ($user->surname == 'Uživatel') ? $user->username : $user->surname;
			$this->addText('user_' . $user->id, 'body pro uživatele ' . $username)->setDefaultValue($this->userHasPointsMapper->loadPointsForUser($this->idExam, $user->id))
				->setAttribute('type', 'number');
		}
		$this->addSubmit('submit');
	}

	/**
	 * @param $values
	 */
	public function save($values) {
		$vals = array();
		$data = $this->subjectService->loadByIdWithAllDependencies($values->idSubject);
		$this->userHasPointsMapper->deleteAllByExam($values->idExam);
		/** @var UserEntity $user */
		foreach ($data->group->users as $user) {
			$uhp = new UserHasPoints();
			$uhp->id_user = $user->id;
			$uhp->id_exam = $values->idExam;
			$uhp->id_subject = $values->idSubject;
			$name = 'user_' . $user->id;
			$uhp->points = $values->$name;
			$this->userHasPointsMapper->insetEntity($uhp);
			$vals[] = $values->$name;
		}
		/** @var Exam $exam */
		$exam = $this->examMapper->loadById($values->idExam);
		$exam->max_points = $values->max_points;
		$exam->name = $values->name;
		$exam->id_subject = $values->idSubject;
		$vals = array_filter($vals);
		$exam->class_avarege = round(array_sum($vals) / count($vals),2);
		$this->examMapper->updateEntity($exam);
	}
}