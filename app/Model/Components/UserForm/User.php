<?php


namespace App\Model\Components\UserForm;


use Kdyby\Translation\Translator;
use MS\Components\BaseControl;
use Nette\Application\UI\Form;

class User extends BaseControl {

	private $userForm;

	public function __construct(\Nette\Security\User $user, Translator $translator, UserForm $userForm) {
		parent::__construct($user, $translator);
		$this->userForm = $userForm;
	}

	public function render() {
		$this->template->setFile(__DIR__ . 'default.latte');
	}

	public function createComponentUserForm() {
		$this->userForm->onValidate[] = [$this, 'UserFormSubmitted'];
		return $this->userForm;
	}

	public function UserFormSubmitted(Form $form, $values) {

	}


}