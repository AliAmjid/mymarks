<?php


namespace App\Model\Components\UserForm;


use Kdyby\Translation\Translator;
use MS\Entity\AclRoleEntity;
use MS\Entity\UserEntity;
use MS\Forms\BaseForm;
use MS\Forms\CustomeFormRenderer;
use MS\Mappers\AclRoleMapper;
use MS\Mappers\UserMapper;
use Nette;

class UserForm extends BaseForm {
	private $userMapper;
	private $aclRoleMapper;

	public function __construct(
		$name = null,
		Nette\Security\User $user,
		CustomeFormRenderer $customeFormRenderer,
		UserMapper $userMapper,
		Translator $translator,
		AclRoleMapper $aclRoleMapper) {
		$this->formName = 'user';
		$this->userMapper = $userMapper;
		$this->aclRoleMapper = $aclRoleMapper;
		parent::__construct($name, null, $customeFormRenderer, $translator, $user);
	}

	public function defineForm() {
		/** @var UserEntity $user */
		$user = $this->userMapper->loadEntityById($_GET['id']);
		$this->addEmail('username', $this->t('username'))->setDefaultValue($user->username);

		$this->addPassword('password', $this->t('password'))
			->setRequired(false)
			->addRule(Nette\Application\UI\Form::MIN_LENGTH, 'Your password has to be at least %d long', 6)
			->setAttribute('minlength', 6);

		$this->addText('class', $this->t('class'))->setDefaultValue($user->class)
			->setAttribute('maxlength', 4);

		if ($this->user->isAllowed('user.changeOtherUsers')) {
			$this->addText('firstname', $this->t('firstname'))->setDefaultValue($user->firstname);
			$this->addText('surname', $this->t('surname'))->setDefaultValue($user->surname);
			$this->addSelect('role', $this->translator->translate('messages.basic.privilageLvl'), $this->loadRoles())->setDefaultValue($this->userMapper->getUserRole($user->id));
		}

		$this->addSubmit('submit');
	}

	private function loadRoles() {
		$roles = array();
		/** @var AclRoleEntity $role */
		foreach ($this->aclRoleMapper->loadAll() as $role) {
			$roles[$role->id] = $this->translator->translate('messages.roles.' . $role->name);
		}
		return $roles;
	}

	public function save($values) {
		/** @var UserEntity $user */
		$user = $this->userMapper->loadEntityById($_GET['id']);
		if ($this->user->isAllowed('user.changeOtherUsers')) {
			$user->firstname = $values->firstname;
			$user->surname = $values->surname;
			$this->userMapper->updateRole($user->id, $values->role);
		}
		$user->username = $values->username;
		$user->password = Nette\Security\Passwords::hash($values->password);
		$user->class = $values->class;
		$this->userMapper->updateEntity($user);
	}
}