module.exports = {
	output: {
		js: 'www/js',
		css: 'www/css'
	},

	jsLibs: [
		'node_modules/jquery/dist/jquery.js'
	]

};
