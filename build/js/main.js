$(document).ready(function () {
	$('[data-toggle="tooltip"]').tooltip();
});

$('.action-delete').click(function () {
	event.preventDefault();
	Swal({
		title: 'Jste si jistí?',
		text: "",
		type: '',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ano, smazat!'
	}).then((result) => {
		if (result.value) {
			document.location = $(this).attr('href');
		}
	});
});

const toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 4500
});

window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-131615469-1');