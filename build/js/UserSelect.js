var UserSelect = function () {
	this.init = function () {
		var element = document.getElementById('selectUser');
		const choices = new Choices(element);
		element.addEventListener('addItem', function (id) {
			document.location = '/user/?id=' + id.detail.customProperties;
		});
		element.addEventListener('search', function (value, resultCount) {
			var request = $.ajax({
				url: "/api/load-users",
				method: "GET",
				data: {query: value.detail.value},
				dataType: "json"
			});

			request.done(function (msg) {
				choices.clearStore();
				choices.setChoices(msg, 'value', 'label', false);
			});

			request.fail(function (jqXHR, textStatus) {
				Swal(
					'Chyba',
					'Omlouváme se, ale došlo k potížím se serverem.',
					'error'
				);
			});
		});
	}
};

$(document).ready(function () {
	if ($('#selectUser')[0]) {
		var userSelect = new UserSelect();
		userSelect.init();
	}
});

$("form").attr('autocomplete', 'off');