var Subject = function () {
	this.vars = {};

	this.initAutoCalculatonOfPoints = function () {
		var _this = this;
		this.getVars();
		$('#frm-addExamForm-max_points').change(function () {
			_this.recalculateAll();
		});
		$('.points-input').change(function () {
			var trForVal = $(this).parent().next();
			var maxPoints = $('#frm-addExamForm-max_points').val();
			var points = $(this).val();
			var percents = (100 / maxPoints) * points;
			trForVal.text(percents + " %");
			var colors = ['#9aff56', '#60a531', '#f3f725', '#efc017', '#ff3700'];
			var mark = 5;
			if (percents > _this.vars.cr_4) {
				mark = 4;
			}
			if (percents > _this.vars.cr_3) {
				mark = 3;
			}
			if (percents > _this.vars.cr_2) {
				mark = 2;
			}
			if (percents > _this.vars.cr_1) {
				mark = 1;
			}
			percents = Math.round(percents);
			trForVal.text(percents + " %" + " => Známka: " + mark);
			var ar = mark - 1;
			trForVal.css('color', colors[ar]);
		});
	};

	this.recalculateAll = function () {
		var _this = this;
		$('.points-input').each(function () {
			var trForVal = $(this).parent().next();
			var maxPoints = $('#frm-addExamForm-max_points').val();
			var points = $(this).val();
			var percents = (100 / maxPoints) * points;
			trForVal.text(percents + " %");
			var colors = ['#9aff56', '#60a531', '#f3f725', '#efc017', '#ff3700'];
			var mark = 5;
			if (percents > _this.vars.cr_4) {
				mark = 4;
			}
			if (percents > _this.vars.cr_3) {
				mark = 3;
			}
			if (percents > _this.vars.cr_2) {
				mark = 2;
			}
			if (percents > _this.vars.cr_1) {
				mark = 1;
			}
			percents = Math.round(percents);
			trForVal.text(percents + " %" + " => Známka: " + mark);
			var ar = mark - 1;
			trForVal.css('color', colors[ar]);
		});
	};

	this.getVars = function () {
		var _this = this;
		var idSubject = $('input[name = "idSubject"]').val();
		$.get("/api/get-subject?idSubject=" + idSubject, function (data, status) {
			_this.vars = data;
			console.log(data);
			_this.recalculateAll();
		});
	}
};

if ($('#edit-exam-grid')) {
	var subject = new Subject();
	subject.initAutoCalculatonOfPoints();
}