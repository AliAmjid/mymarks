var Group = function () {
	this.table = $('#create-group-grid');
	this.dataTable = $(this.table).DataTable();
	this.init = function () {
		this.dataTable = $(this.table).DataTable();
		this.initUserSelect();
		this.handleRename();
		this.invite();
	};


	this.initUserSelect = function () {
		const _this = this;
		var element = document.getElementById('select-user-for-grid');
		const choices = new Choices(element);
		element.addEventListener('addItem', function (id) {
			_this.handleGrid(id);
			choices.clearStore();
		});
		element.addEventListener('search', function (value, resultCount) {
			var request = $.ajax({
				url: "/api/load-users",
				method: "GET",
				data: {query: value.detail.value},
				dataType: "json"
			});

			request.done(function (msg) {
				choices.clearStore();
				choices.setChoices(msg, 'value', 'label', false);
			});

			request.fail(function (jqXHR, textStatus) {
				Swal(
					'Chyba',
					'Omlouváme se, ale došlo k potížím se serverem.',
					'error'
				);
				console.log(jqXHR);
				console.log(textStatus);
			});
		});
	};

	this.handleGrid = function (user) {
		const _this = this;
		var request = $.ajax({
			url: "/group/add-user",
			method: "GET",
			data: {
				idUser: user.detail.customProperties,
				idGroup: $('#data-group-id').text()
			},
			dataType: "json"
		});

		request.done(function (data) {
			if (data.ok == true) {
				_this.dataTable.row.add(data.user).draw();
			} else {
				swal('Chyba', data.msg, 'error');
			}
		});

		request.fail(function (jqXHR, textStatus) {
			Swal(
				'Chyba',
				'Omlouváme se, ale došlo k potížím se serverem.',
				'error'
			);
			console.log(jqXHR);
			console.log(textStatus);
		});
	};

	this.handleRename = function () {
		$('#group-name').blur(function () {
			console.log('zmena');
			var request = $.ajax({
				url: "/group/rename",
				method: "GET",
				data: {
					idGroup: $('#data-group-id').text(),
					name: $('#group-name').val()
				},
				dataType: "json"
			});

			request.done(function (data) {
				if (data.ok == true) {
					$('#group').text(data.msg);
				} else {
					swal('Chyba', data.msg, 'error');
				}
			});
		});
	};

	this.invite = function () {
		$('#invite-new-user-form').submit(function () {
			event.preventDefault();
			var email = $('#invite-new-user').val();
			var _this = this;
			console.log(email);
			if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{1,6})+$/.test(email)) {
				swal('Špatný formát emailu', '', 'error');
			} else {
				/*	var swal = Swal({
						title: 'Jste si jistí?',
						html: "Chystáte se založit abstraktního uživatele pro email <b>" + email + "</b>.<br> Po registraci pod tímto emailem uživatel automaticky získa všechny změny (známky,řazení do skupin), které byly učiněny.<br>Abstraktní uživatel se chová stejně jako reálný",
						type: '',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Ano, poslat pozvánku'
					}).then((result) => {
						if (result.value) {

						}
					})*/
				toast('Toto bude chvilku trvat..', '', 'info');
				$('#invite-new-user').val('');
				var request = $.ajax({
					url: "/api/create-abstract-user",
					method: "GET",
					data: {email: email},
					dataType: "json"
				});

				request.done(function (data) {
					if (data.ok == true) {
						toast('Úspěch', data.msg, 'success');
						var user = {
							detail: {
								customProperties: data.uid
							}
						};
						var createGroup = new Group();
						createGroup.handleGrid(user);
					} else {
						swal('Chyba', data.msg, 'error');
					}
				});

				request.fail(function (jqXHR, textStatus) {
					Swal(
						'Chyba',
						'Omlouváme se, ale došlo k potížím se serverem.',
						'error'
					);
					console.log(jqXHR);
					console.log(textStatus);
				});

			}
		});
	};
};

$(document).ready(function () {
	if ($('#create-group-grid')[0]) {
		var createGroup = new Group();
		createGroup.init();
	}
	$('.data-grid').not('.disabled-sorting').each(function () {
		$(this).DataTable({
			"lengthMenu": [ 50, 75, 100 ]
		});
	});


	if ($('.disabled-sorting')) {
		$('.disabled-sorting').DataTable({"ordering": false,"lengthMenu": [ 50, 75, 100 ]});
	}
});