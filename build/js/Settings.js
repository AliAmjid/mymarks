var Settings = function () {

	this.init = function () {

		$('.switch').click(function () {
			var _this = this;
			var request = $.ajax({
				url: "/settings/switch",
				method: "GET",
				data: {
					idResource: $(this).data('resource'),
					idRole: $(this).data('role')
				},
				dataType: "json"
			});

			request.done(function (data) {
				if (data.ok == true) {
					toast('Úspěch', data.msg, 'success');
					$(_this).removeClass('disabled');
					$(_this).removeClass('enabled');
					$(_this).addClass(data.class);
					console.log(data.class);
					console.log(_this);

				} else {
					swal('Chyba', data.msg, 'error');
				}
			});

			request.fail(function (jqXHR, textStatus) {
				Swal(
					'Chyba',
					'Omlouváme se, ale došlo k potížím se serverem.',
					'error'
				);
				console.log(jqXHR);
				console.log(textStatus);
			});
		});
	}
};

if($('.settings-grid')) {
	var settings = new Settings();
	settings.init();
}
