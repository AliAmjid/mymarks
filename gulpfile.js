var paths = {
	output: {
		js: 'www/js',
		css: 'www/css'
	},

	jsLibs: [
		'node_modules/tether/dist/js/tether.min.js',
		'node_modules/jquery/dist/jquery.js',
		'node_modules/bootstrap/dist/js/bootstrap.js',
		'www/assets/js/core/popper.min.js',
		'www/assets/js/plugins/bootstrap-notify.js',
		'www/assets/js/plugins/chartjs.min.js',
		'www/assets/js/plugins/perfect-scrollbar.jquery.min.js',
		'www/assets/js/paper-dashboard.js',
		'node_modules/sweetalert2/dist/sweetalert2.all.min.js',
		'node_modules/datatables.net/js/jquery.dataTables.min.js',
		'node_modules/datatables.net-dt/js/dataTables.dataTables.min.js',
		'node_modules/choices.js/public/assets/scripts/choices.min.js',
		'node_modules/chart.js/dist/Chart.bundle.min.js'
	],

	cssLibs: [
		'node_modules/bootstrap/dist/css/bootstrap.css',
		'www/assets/css/paper-dashboard.css',
		'node_modules/tether/dist/css/tether.css',
		'node_modules/@fortawesome/fontawesome-free/css/all.css',
		'node_modules/sweetalert2/dist/sweetalert2.all.min.css',
		'node_modules/datatables.net-dt/css/jquery.dataTables.min.css',
		'node_modules/choices.js/public/assets/styles/base.min.css',
		'node_modules/choices.js/public/assets/styles/choices.min.css'
	]

};

var fs = require("fs");
var version = fs.readFileSync('www/version', 'utf8', function(err, data) {
	return data;
});

var gulp = require('gulp');
var addsrc = require('gulp-add-src');
var concat = require('gulp-concat');
var ts = require('gulp-typescript');
var uglify = require('gulp-uglify-es').default;
var minifyCSS = require('gulp-minify-css');
var less = require('gulp-less');
var gutil = require('gulp-util');
var watch = require('gulp-watch');


gulp.task('jsLib', function () {
	gulp.src(paths.jsLibs)
		.pipe(concat("libs-" + version + ".js"))
		.pipe(uglify())
		.on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
		.pipe(gulp.dest('www/js'));
});

gulp.task('cssLib', function () {
	gulp.src(paths.cssLibs)
		.pipe(minifyCSS())
		.pipe(concat("libs-" + version + ".css"))
		.pipe(gulp.dest('www/css'));
});

gulp.task('less', function () {
	gulp.src('build/css/*.less')
		.pipe(less())
		.pipe(minifyCSS())
		.pipe(concat("release-" + version + ".css"))
		.pipe(gulp.dest('www/css'));
});

gulp.task('js', function () {
	gulp.src('build/js/*.js')
		.pipe(concat("release-" + version + ".js"))
		.pipe(uglify())
		.pipe(gulp.dest('www/js'));
});

gulp.task('default',function () {
	gulp.run('jsLib','cssLib','less','js');
});

gulp.task('watch',function () {
	gulp.run('jsLib','cssLib','less','js');
});

gulp.task('watch', function () {
	gulp.run('default');
	gulp.watch('build/css/*.less', ['less']);
	gulp.watch('build/js/*.js', ['js']);
});