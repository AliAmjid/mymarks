
SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `acl_resource`;
CREATE TABLE `acl_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `acl_resource` (`id`, `name`) VALUES
(1,	'dashboard'),
(2,	'user.changeOtherUsers'),
(3,	'user'),
(4,	'group'),
(5,	'group.edit'),
(6,	'user.create'),
(7,	'group.editAll'),
(8,	'subject'),
(9,	'settings');

DROP TABLE IF EXISTS `acl_role`;
CREATE TABLE `acl_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `acl_role` (`id`, `name`) VALUES
(1,	'teacher'),
(2,	'developer'),
(3,	'student');

DROP TABLE IF EXISTS `acl_role_has_resource`;
CREATE TABLE `acl_role_has_resource` (
  `id_acl_resource` int(11) NOT NULL,
  `id_acl_role` int(11) NOT NULL,
  KEY `fk_acl_role_has_resource_acl_resource_idx` (`id_acl_resource`),
  KEY `fk_acl_role_has_resource_acl_role1_idx` (`id_acl_role`),
  CONSTRAINT `fk_acl_role_has_resource_acl_resource` FOREIGN KEY (`id_acl_resource`) REFERENCES `acl_resource` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_acl_role_has_resource_acl_role1` FOREIGN KEY (`id_acl_role`) REFERENCES `acl_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `acl_role_has_resource` (`id_acl_resource`, `id_acl_role`) VALUES
(9,	2),
(2,	1),
(3,	1),
(1,	2),
(4,	2),
(5,	2),
(7,	2),
(8,	2),
(3,	2),
(2,	2),
(6,	2),
(1,	1),
(4,	1),
(5,	1),
(8,	1),
(6,	1),
(1,	3),
(3,	3);

DROP TABLE IF EXISTS `enum`;
CREATE TABLE `enum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_key` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `exam`;
CREATE TABLE `exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_subject` int(11) NOT NULL,
  `max_points` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `class_avarege` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_exam_subject1_idx` (`id_subject`),
  CONSTRAINT `fk_exam_subject1` FOREIGN KEY (`id_subject`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `copied_from` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_group_user1_idx` (`created_by`),
  KEY `fk_group_group1_idx` (`copied_from`),
  CONSTRAINT `fk_group_group1` FOREIGN KEY (`copied_from`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_group_user1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `invitation`;
CREATE TABLE `invitation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `has_abstract_user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_invitations_user1_idx` (`has_abstract_user`),
  CONSTRAINT `fk_invitations_user1` FOREIGN KEY (`has_abstract_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `claims` int(11) DEFAULT NULL,
  `id_group` int(11) NOT NULL,
  `class_average` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `cr_1` int(11) DEFAULT '85' COMMENT 'cr = convert_rate for final mark',
  `cr_2` int(11) DEFAULT '70',
  `cr_3` int(11) DEFAULT '55',
  `cr_4` int(11) DEFAULT '40',
  PRIMARY KEY (`id`),
  KEY `fk_subject_user1_idx` (`claims`),
  KEY `fk_subject_group1_idx` (`id_group`),
  CONSTRAINT `fk_subject_group1` FOREIGN KEY (`id_group`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_subject_user1` FOREIGN KEY (`claims`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `isAbstract` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `user` (`id`, `username`, `password`, `firstname`, `surname`, `class`, `isAbstract`) VALUES
(1,	'Admin',	'$2y$10$U1EIokh8uiWosPo7a1nPFelf8U94o68stljQPXsIezuyotM9BwllK',	'Admin',	'Admin',	NULL,	0);

DROP TABLE IF EXISTS `user_has_group`;
CREATE TABLE `user_has_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_group` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_has_group_user1_idx` (`id_user`),
  KEY `fk_user_has_group_group1_idx` (`id_group`),
  CONSTRAINT `fk_user_has_group_group1` FOREIGN KEY (`id_group`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_group_user1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `user_has_points`;
CREATE TABLE `user_has_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_exam` int(11) NOT NULL,
  `points` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_subject` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_has_points_exam1_idx` (`id_exam`),
  KEY `fk_user_has_points_user1_idx` (`id_user`),
  KEY `fk_user_has_points_subject1_idx` (`id_subject`),
  CONSTRAINT `fk_user_has_points_exam1` FOREIGN KEY (`id_exam`) REFERENCES `exam` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_points_subject1` FOREIGN KEY (`id_subject`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_points_user1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `user_has_role`;
CREATE TABLE `user_has_role` (
  `id_user` int(11) NOT NULL,
  `id_acl_role` int(11) NOT NULL,
  KEY `fk_user_has_role_user1_idx` (`id_user`),
  KEY `fk_user_has_role_acl_role1_idx` (`id_acl_role`),
  CONSTRAINT `fk_user_has_role_acl_role1` FOREIGN KEY (`id_acl_role`) REFERENCES `acl_role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_role_user1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `user_has_role` (`id_user`, `id_acl_role`) VALUES
(1,	2),
(1,	3);

-- 2019-01-01 16:15:55